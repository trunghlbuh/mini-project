import { Component, OnInit } from '@angular/core';
import { Matrix } from '../models/matrix.models';

@Component({
  selector: 'app-matrix',
  templateUrl: './matrix.component.html',
  styleUrls: ['./matrix.component.scss']
})
export class MatrixComponent implements OnInit {
  numberArr: Matrix[] = [];
  numberItem = 10;
  isPrime: boolean = true;
  constructor() { }

  ngOnInit(): void {
    for (let i = 1; i <= 100; i++) {
      this.numberArr.push({ item: i, selected: false });
    }
  }

  /**
   * Handle when click number on matrix table
   */
  onSelect(itemSelected: Matrix): void {
    itemSelected.selected = true;
  }

  /**
   * Check number is prime or not
   * @param number is selected
   * @returns boolean
   */
  isCheckPrime(number: number): boolean {
    for (let i = 2, s = Math.sqrt(number); i <= s; i++)
      if (number % i === 0) return false;
    return number > 1;
  }
}
